const rectangleWidth = 500;
const rectangleLength = 50;
const rectangleArea = rectangleWidth * rectangleLength;
console.log(`${rectangleArea} = ${rectangleWidth} * ${rectangleLength}`);